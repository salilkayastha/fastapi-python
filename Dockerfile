# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /src

RUN useradd --no-create-home -r -s /usr/sbin/nologin geoip && chown -R geoip /src

COPY --chown=geoip:geoip requirements.txt requirements.txt

RUN pip3 install --upgrade --user pip
RUN pip3 install -r requirements.txt

COPY --chown=geoip:geoip ./app app
RUN mkdir -p /opt/geoip/logs/
RUN chown -R geoip /opt/geoip/

EXPOSE 8000

USER geoip

CMD ["python3", "-m" , "uvicorn", "app:app", "--host", "0.0.0.0", "--date-header"]
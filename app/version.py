author_info = (
    ("Paulo Roberto", "paulo.rb.beserra@gmail.com"),
)

project_home = ""

package_info = ""
package_license = "MIT"

team_email = "paulo.rb.beserra@gmail.com"

version_info = (0, 6)

__author__ = ", ".join("{} <{}>".format(*info) for info in author_info)
__version__ = ".".join(map(str, version_info))
